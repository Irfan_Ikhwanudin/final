<?php
use App\Http\Controllers\SessionsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\JawabanController;
use App\Pertanyaan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Session
Route::get('/login', [SessionsController::class, 'bflogin'])->name('login');
Route::post('/login/login', [SessionsController::class, 'login']);
Route::get('/logout', [SessionsController::class, 'logout']);
Route::get('/register', [SessionsController::class, 'signup']);
Route::post('/register/create', [SessionsController::class, 'create']);


Route::middleware(['auth'])->group(function(){


    Route::get('/index', function(){ return view('index');});
    Route::get('/profile', [HomeController::class, 'profile']);



    Route::get('/',[HomeController::class, 'home']);
    // testing master template 
    Route::get('/master', function(){
        return view('layouts.master');
    });
    /* CRUD Pertanyaan */
    // Create
    // Form Pertanyaan
    Route::get('/pertanyaan/create',[PertanyaanController::class,'create']);
    // Kirim data ke database atau tambah data ke database
    Route::post('/pertanyaan',[PertanyaanController::class,'store']);


    // Read Data
    // Tampil Semua data di blade

    Route::get ('/pertanyaan', [PertanyaanController::class,'index']);

    // Detail Pemeran Berdasarkan Id
    Route::get('/pertanyaan/{pertanyaan_id}',[PertanyaanController::class,'show']);

    // Update
    // Form update
    Route::get('/pertanyaan/{pertanyaan_id}/edit',[PertanyaanController::class,'edit']);

    // Update data ke database berdasar Id
    Route::put('/pertanyaan/{pertanyaan_id}',[PertanyaanController::class,'update']);

    // Delete
    // Delete Berdasarkan parameter Id
    Route::delete('/pertanyaan/{pertanyaann_id}',[PertanyaanController::class,'destroy']);

    // CRUD Tanya Jawab
    Route::resource('pertanyaan', PertanyaanController::class);
    Route::resource('jawaban', JawabanController::class);
    Route::resource('kategori', KategoriController::class);

});



