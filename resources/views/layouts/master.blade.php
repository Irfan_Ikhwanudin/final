<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>
  <link rel="shortcut icon" type="image/png" href="{{asset('/template2/src/assets/images/logos/favicon.png')}}"/>
  <link rel="stylesheet" href="{{asset('/template2/src/assets/css/styles.min.css')}}" />
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  @stack('headers')
</head>

<body>
    <!-- partial:partials/_navbar.html -->
    @include('layouts.partial.nav')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      @include('layouts.partial.sidebar')
      <!-- partial -->
       <!--  Main wrapper -->

  <!-- Content Wrapper. Contains page content -->
  <div class="body-wrapper">
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
          <section class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1> @yield('title')</h1>
              </div>
            </div>
          </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
        <!-- Default box -->
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">@yield('title')</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
            <div class="main-panel">
              <div class="content-wrapper">
                @yield('content')
             </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
            </div>
          <!-- /.card-footer-->
          </div>
        </section>
     </div>     
   </div>
 <!-- /.content -->



  <!-- container-scroller -->
  <script src="{{asset('/template2/src/assets/libs/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{asset('/template2/src/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('/template2/src/assets/js/sidebarmenu.js')}}"></script>
  <script src="{{asset('/template2/src/assets/js/app.min.js')}}"></script>
  <script src="{{asset('/template2/src/assets/libs/apexcharts/dist/apexcharts.min.js')}}"></script>
  <script src="{{asset('/template2/src/assets/libs/simplebar/dist/simplebar.js')}}"></script>
  <script src="{{asset('/template2/src/assets/js/dashboard.js')}}"></script>
@stack('script')
</body>
</html>