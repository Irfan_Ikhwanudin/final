<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <meta name="title" content="Ask online Form">
  <meta name="description" content="The Ask is a bootstrap design help desk, support forum website template coded and designed with bootstrap Design, Bootstrap, HTML5 and CSS. Ask ideal for wiki sites, knowledge base sites, support forum sites">
  <meta name="keywords" content="HTML, CSS, JavaScript,Bootstrap,js,Forum,webstagram ,webdesign ,website ,web ,webdesigner ,webdevelopment">
  <meta name="robots" content="index, nofollow">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="language" content="English">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
   <link rel="stylesheet" href="{{asset('/template/css/loginstyle.css')}}">

  
</head>

<body>
  
<div class="modal-wrap">

  <div class="modal-bodies">
    <div class="modal-body modal-body-step-1 is-showing">
      <div class="title">Login</div>
      <div class="description">Hello there, login here</div>
    
      <form action="/home" method="post">
        @csrf
        <input type="text" placeholder="username"/>
        <input type="email" placeholder="email"/>
          <input type="password" placeholder="password"/>
            <input type="con-password" placeholder="con-password"/>
               <div class="col-md-4">
                        <div class="row text-center sign-with">
                        </div>
                    </div>
        <div class="text-center">
            <input type="submit" value="Submit"><br>
        </div>
      </form>
    </div>

 
  </div>
</div>
  <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>


</body>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="title" content="Ask online Form">
    <meta name="description"
        content="The Ask is a bootstrap design help desk, support forum website template coded and designed with bootstrap Design, Bootstrap, HTML5 and CSS. Ask ideal for wiki sites, knowledge base sites, support forum sites">
    <meta name="keywords"
        content="HTML, CSS, JavaScript,Bootstrap,js,Forum,webstagram ,webdesign ,website ,web ,webdesigner ,webdevelopment">
    <meta name="robots" content="index, nofollow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href={{ '../forum/css/loginstyle.css' }}>


</head>

<body>

    <div class="modal-wrap">

        <div class="modal-bodies">
            <div class="modal-body modal-body-step-1 is-showing">
                <div class="title">Sign Up</div>
                <div class="description">Hello there, Register Form</div>
                <form action="/register/create" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{ Session::get('name') }}" placeholder="name" />
                    </div>
                    <div class="mb-3">
                        <label for="email">Email</label>
                    <input type="email" name="email" value="{{ Session::get('email') }}" placeholder="E-Mail" />
                </div>
                <div class="mb-3">
                    <label for="password">Password</label>
                    <input type="password" name="password" placeholder="Password" />
                </div>
                    <div class="text-center">
                        <a href="/login">
                            <p>Already have an account? Sign in now</p>
                        </a>
                        <button class="btn btn-danger" type="submit" name="submit">Register</button>

                    </div>
                </form>
            </div>


        </div>
    </div>
    <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>

</body>

</html>
