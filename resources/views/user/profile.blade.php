@include('assets/header')
    <!-- ======breadcrumb ======-->
    <section class="header-descriptin329">
        <div class="container">
            <h3>User Deatils</h3>
            <ol class="breadcrumb breadcrumb839">
                <li><a href="#">Home</a></li>
                <li class="active">User Details</li>
            </ol>
        </div>
    </section>
    <section class="main-content920">
        <div class="container">
            <div class="row">
                <!--    body content-->
                <div class="col-md-9">
                    <div class="about-user2039 mt-70">
                        <div class="user-title3930">
                            <h3>About <a href="#">{{Auth::user()->name}}</a>
                     
                        <span class="badge229">
                        <a href="#">punit</a></span>
                        </h3>
                            <hr> </div>
                        <div class="user-image293"> <img src="image/images.png" alt="Image"> </div>
                        <div class="user-list10039">
                            <div class="ul-list-user-left29">
                                <ul>
                                    <li><i class="fa fa-plus" aria-hidden="true"></i> <strong>Registered:</strong> {{Auth::user()->created_at}}</li>
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> <strong>Country:</strong> Egypt</li>
                                    <li><i class="fa fa-heart" aria-hidden="true"></i> <strong>Age:</strong> 27</li>
                                    <li><i class="fa fa-globe" aria-hidden="true"></i> <strong>email:</strong><a href="#"> {{Auth::user()->email}}</a></li>
                                </ul>
                            </div>
                            <div class="ul-list-user-right29">
                                <ul>
                                    <li><i class="fa fa-phone" aria-hidden="true"></i> <strong>Phone:</strong> 01111111110</li>
                                    <li><i class="fa fa-globe" aria-hidden="true"></i> <strong>City:</strong> Cairo</li>
                                    <li><i class="fa fa-user" aria-hidden="true"></i> <strong>Sex: </strong>Male</li>
                                </ul>
                            </div>
                        </div>
                        <div class="user-description303">
                            <p>Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur vitae velit in neque dictum blandit.</p> <a href="#">Ask  {{Auth::user()->name}}</a> </div>
                        <div class="user-social3903">
                            <p>Follow : <span>
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                        
                    </span> </p>
                        </div>
                    </div>
                    <div class="user-statas921">
                       <div class="row">
                       <div class="col-md-6">
                        <div class="ul_list_ul_list-icon-ok281">
                            <ul>
                                <li><a href="#">Questions ( 4 )</a></li>
                                <li><a href="#">Asked Questions ( 0 )</a></li>
                                <li><a href="#">Followed Questions ( 3 )</a></li>
                                <li><a href="#">Posts ( 10 )</a></li>
                                <li><a href="#">Points ( 208 )</a></li>
                                <li><a href="#">Followers ( 47 )</a></li>
                                <li><a href="#">Comments ( 1 )</a></li>
                            </ul>
                        </div>
                           </div>
                           <div class="col-md-6">
                        <div class="ul_list_ul_list-icon-ok281">
                            <ul>
                                <li><a href="#">Questions ( 4 )</a></li>
                                <li><a href="#">Asked Questions ( 0 )</a></li>
                                <li><a href="#">Followed Questions ( 3 )</a></li>
                                <li><a href="#">Posts ( 10 )</a></li>
                                <li><a href="#">Points ( 208 )</a></li>
                                <li><a href="#">Followers ( 47 )</a></li>
                                <li><a href="#">Comments ( 1 )</a></li>
                            </ul>
                        </div>
                           </div>
                        </div>
                    </div>
                    
                </div>
                <!--                end of col-md-9 -->
                <!--           strart col-md-3 (side bar)-->
                @include('assets/side')
                    <!--       end recent post    -->
                </aside>
            </div>
        </div>
    </section>
    @include('assets/footer')