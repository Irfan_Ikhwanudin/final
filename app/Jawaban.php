<?php

namespace App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    use HasFactory;
    protected $table = 'jawaban';
    
    protected $fillable = ['jawaban', 'gambar', 'pertanyaan_id', 'users_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }

    public function pertanyaan()
    {
        return $this->belongsTo('App\Pertanyaan', 'pertanyaan_id');
    }
}
