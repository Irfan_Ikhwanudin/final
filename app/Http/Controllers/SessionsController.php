<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class SessionsController extends Controller
{
    function index(){
        return redirect('index');
    }
    function bflogin(){
        return view('login');
    }
    function login(Request $request){
        Session::flash('email', $request->email);
        $request->validate([
            'email'=>'required',
            'password'=>'required'
        ],[
            'email.required'=>'Email wajib diisi!!!',
            'password.required'=>'Password wajib diisi!!!',
        ]);
        $login = [
            'email'=>$request->email, 
            'password'=>$request->password, 
        ];
        if(Auth::attempt($login)){
            $users = User::all();
            return redirect('index', 302)->with(compact('users'));
        }else{
            return redirect('/login')->withErrors('User atau password salah!!');
        }
    }
    function signup(){
        return view('register');
    }
    function create(Request $request){
        Session::flash('email', $request->email);
        Session::flash('name', $request->name);
        $request->validate([
            'name' => 'required',
            'email'=> 'required|email|unique:users',
            'password'=> 'required|min:6',
        ],[
            'email.required'=>'Email wajib diisi!!!',
            'email.unique'=>'Email telah terdaftar!!!',
            'email.email'=>'Email tidak valid!!!',
            'name.required'=>'name wajib diisi!!!',
            'password.required'=>'Password wajib diisi!!!',
            'password.min'=>'Password minimal 6 karakter!!!',
        ]);
        $data = [
            'name' => $request->name,
            'password' => Hash::make($request->password),
            'email' => $request->email,
        ];
       User::create($data);
        $login = [
            'email'=>$request->email, 
            'password'=>$request->password, 
        ];
        if(Auth::attempt($login)){
            return redirect('index')->with('success', Auth::user()->name.'behasil login');
        }else{
            // return '!ok';
            return redirect('/login')->withErrors('User atau password salah!!');
        }
    }
    function logout(){
        Auth::logout();
        return redirect('/login');
    }
}
