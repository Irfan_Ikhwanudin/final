<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    

    public function submit(request $request){
        $username = $request->input('username');
        $email = $request->input('email');
        $password = $request->input('password');
        $con = $request->input('con-password');
        return view ('penutup',['username'=> $username,'email'=>$email,'password'=> $password, 'con-password'=> $con]);
}
}