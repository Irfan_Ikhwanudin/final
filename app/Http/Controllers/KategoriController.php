<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    public function create()
    {
        return view('kategori.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_kategori' => 'required',
        ]);
        DB::table('kategori')->insert([
            'nama_kategori' => $request['nama_kategori'],
        ]);
        return redirect('/kategori');

    }
    public function index(){
        $kategori = DB::table('kategori')->get();
        return view('kategori.index', ['kategori' => $kategori]);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $kategori = DB::table('kategori')->find($id);
        return view('kategori.show', ['kategori' =>$kategori]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategori')->find($id);
        return view('kategori.edit', ['kategori' =>$kategori]);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_kategori' => 'required',
        ]);
        
        $kategori = DB::table('kategori')
        ->where('id', $id)
        ->update(['nama_kategori' => $request->nama_kategori]);
  
        return redirect('/kategori');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        {
            DB::table('kategori')->where('id', $id)->delete();
            return redirect('/kategori');
            }
    }
}
