<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Kategori;
use App\Pertanyaan;
use App\Models\User;
use File;
Use Alert;



class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $pertanyaan = Pertanyaan::all();
        return view('pertanyaan.index', ['pertanyaan'=>$pertanyaan, 'users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();

        return view('pertanyaan.create', ['kategori'=>$kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'gambar' => 'required|mimes:png,jpeg,jpg|max:2048',
        ]);

        $id = Auth::id();
  
        $namaGambar = time().'.'.$request->gambar->extension();  
   
        $request->gambar->move(public_path('images'), $namaGambar);

        $pertanyaan = new Pertanyaan;
 
        $pertanyaan->judul = $request->judul;
        $pertanyaan->content = $request->content;
        $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->users_id = $id;
        $pertanyaan->gambar = $namaGambar;
        
        $pertanyaan->save();
        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $kategori = Kategori::all();
        return view('pertanyaan.show', ['pertanyaan' =>$pertanyaan, 'kategori' =>$kategori]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $kategori = Kategori::all();
        return view('pertanyaan.edit', ['pertanyaan' =>$pertanyaan, 'kategori' =>$kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'gambar' => 'mimes:png,jpeg,jpg|max:2048',
        ]);

        $pertanyaan = Pertanyaan::find($id);
        if($request->has('gambar')) {
            $path = "images/";


            File::delete($path . $pertanyaan->gambar);

            $namaGambar = time().'.'.$request->gambar->extension();  
   
            $request->gambar->move(public_path('images'), $namaGambar);  

            $pertanyaan->gambar = $namaGambar;

            $pertanyaan->save();
        }

        $pertanyaan->judul = $request->judul;
        $pertanyaan->content = $request->content;
        $pertanyaan->kategori_id = $request->kategori_id;

        $pertanyaan->save();
        return redirect('/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $path = "images/";
            File::delete($path . $pertanyaan->gambar);
        $pertanyaan->delete();
    }
    
 
}
