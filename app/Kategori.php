<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    use HasFactory;
    protected $table = "kategori";
     
    protected $fillable = ["nama_kategori"];

    public function pertanyaan()
    {
        return $this->hasMany('App\Pertanyaan', 'kategori_id');
    }
}
