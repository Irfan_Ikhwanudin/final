<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DummyUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userData = [
        [
            'name' => 'John',
            'email' => 'user@duck.com',
            'password' =>bcrypt('12345678')
        ]
        ];
        foreach($userData as $key => $val){
            User::create($val);
        }
    }
}
